import numpy as np
from numba import jitclass
from numba import uint64, int64

spec = [
    ('keys', uint64[:]),
    ('vals', int64[:]),
    ('index', uint64[:]),
    ('lastItem', int64),#int: -1 if empty
    ('key', uint64),
    ('val', int64),
]


@jitclass(spec)
class Heap(object):

    def __init__(self, size):
        self.index = np.arange(size)
        self.keys = np.arange(size)
        self.vals = np.full(size, np.iinfo(np.int64).max, dtype = np.int64)
        self.lastItem = -1
    
    def siftdown(self, i, m):
        while i*2+1 <= m:
            #Gibt es zwei Kinder?
            if i*2+2 <=m:
                if self.vals[i*2+1] > self.vals[i*2+2]:
                    j = i*2+2
                else:
                    j = i*2+1
            else:
                j = i*2+1
            
            if self.vals[i] > self.vals[j]:
                self.index[self.keys[i]] = j
                self.index[self.keys[j]] = i
                
                tempKey = self.keys[i]
                tempVal = self.vals[i]
                
                self.vals[i] = self.vals[j]
                self.keys[i] = self.keys[j]
                self.vals[j] = tempVal
                self.keys[j] = tempKey

                i = j
            else:
                return
    
    def siftup(self, j):
#        print("siftup")
        i = np.int64(j)
        parent = np.int64(int((i+1)/2)-1)
        while parent >= 0:
            if self.vals[parent] > self.vals[i]:
                self.index[self.keys[parent]] = i
                self.index[self.keys[i]] = parent
                
                tempKey = self.keys[i]
                tempVal = self.vals[i]
                
                self.vals[i] = self.vals[parent]
                self.keys[i] = self.keys[parent]
                self.vals[parent] = tempVal
                self.keys[parent] = tempKey
                
                i = parent
                parent = np.int64(int((i+1)/2)-1)
            else:
                return

    def decreasePriority(self, key, p):
        assert(self.vals[self.index[key]]>= p)
        if self.index[key] <= self.lastItem:
            self.vals[self.index[key]] = p
            self.siftup(self.index[key])
        else:
            self.reactivate(key, p)

    def min(self):
        if self.lastItem == -1:
            return np.int64(-1), np.int64(-1)
        else:
            return np.int64(self.keys[0]), np.int64(self.vals[0])

    def pop(self):
        if self.lastItem == -1:
            return -1, -1

        oKey = np.int64(self.keys[0])
        oVal = np.int64(self.vals[0])
        
        self.index[self.keys[0]] = self.lastItem

        self.keys[0] = self.keys[self.lastItem]
        self.vals[0] = self.vals[self.lastItem]
        
        self.index[self.keys[self.lastItem]] = 0
        self.keys[self.lastItem] = oKey
        self.vals[self.lastItem] = oVal
        
        self.lastItem -= 1
        
        self.siftdown(0, self.lastItem)
        
        return oKey, oVal
        
    def getVal(self, key):
        return self.vals[self.index[key]]
        
    def isActive(self, key):
        return self.index[key] <= self.lastItem
        
    def size(self):
        return self.lastItem+1
        
    def reactivate(self, key, val):
        assert(self.index[key] > self.lastItem)
        oIndex = self.lastItem+1
        oKey = self.keys[self.lastItem+1]
        oVal = self.vals[self.lastItem+1]
        
        self.index[self.keys[self.lastItem+1]] = self.index[key]
        self.keys[self.lastItem+1] = self.keys[self.index[key]]
        self.vals[self.lastItem+1] = self.vals[self.index[key]]
        
        self.keys[self.index[key]] = oKey
        self.vals[self.index[key]] = oVal
        self.index[key] = oIndex
        
        self.lastItem += 1
        
        self.vals[self.index[key]] = val
        
        assert(self.index[key] == self.lastItem)
        self.siftup(self.index[key])
        
    def reset(self):
        self.vals.fill(np.iinfo(np.int64).max)
        self.lastItem = -1

