import numpy as np
from bitarray import bitarray
from numba import njit, uint64, int64
from heap import Heap
import datetime

def optimize(kmer_pages, pagesize, pagecount):
    nkmers = kmer_pages.shape[0]
    #Array initial mit nkmer+pagecount befüllt, alle nicht zugewiesenen plätze zeigen auf die Senke
    page_kmers = np.full((pagecount, pagesize), nkmers, dtype=np.uint64)
    pF = np.zeros(pagecount, dtype=np.uint64)

    
    # Wenn eine Kante benutzt wurde wird das Bit auf 1 gesetzt, jedes kmer hat 3 bits für die Hashfunktionen
    ukmerr = bitarray(nkmers*3, val=0)
    ukmer = ukmerr.array
    get_bits_at = ukmerr.get  # (array, startbit, nbits=1)
    set_bits_at = ukmerr.set
 
    @njit()
    def _init(pk, pf, set_bits_at, get_bits_at, ukmer, kmer_pages):
        for i in range(nkmers):
            assert(int(get_bits_at(ukmer, i*3, 3)) == 0)
            if pf[kmer_pages[i][0]] != pagesize:
                j = kmer_pages[i][0]
                pk[j][pf[j]] = i
                pf[j] += 1
                set_bits_at(ukmer, i*3, 1)
        count = 0
        for i in range(kmer_pages.shape[0]):
            if int(get_bits_at(ukmer, i*3, 3)) == 0:
                if pf[kmer_pages[i][1]] != pagesize:
                    j = kmer_pages[i][1]
                    pk[j][pf[j]] = i
                    pf[j] += 1
                    set_bits_at(ukmer, i*3+1, 1)
                else:
                    count += 1

    @njit()
    def _getPath(heap, prev_k, prev_p, start):
        node = prev_p[pagecount]
        path = []
        path.append(prev_p[pagecount])
        while prev_p[node] != -1:
            path.append(prev_k[node])
            path.append(prev_p[node])
            node = prev_p[node]
        path.append(start)
        return path

    def _checkPath(path, prev_p, prev_k):

        correct = True
        #assert(len(path) % 2 == 0)

        for j in range(len(path)-1):
            i = len(path)-1 -j
            if i % 2 == 1:
                correct = correct and path[i-1] in kmer_pages[path[i]]
            else:
                #assert(prev_k[path[i]] == path[i+1])
                #assert(i == len(path)-2 or prev_p[path[i]] == path[i+2])
                correct = correct and path[i-1] in page_kmers[path[i]]
        if not correct:
            print("path nicht möglich")

    @njit()# ToDo: direkt auf den vorgängern
    def _alternatePath(path, kmer_pages, page_kmers, ukmer, setB):
        for j in range(len(path)):
            i = len(path)-1 -j
            if i % 2 == 1:#bon kmer i zu page i-1
                setB(ukmer, path[i]*3, 0, 3)
                hfunc = -1
                for k, page in enumerate(kmer_pages[path[i]]):
                    if page == path[i-1]:
                        hfunc = k
                        break
                #assert(hfunc != -1)
                setB(ukmer, path[i]*3+hfunc, 1)

            else:
                kPos = -1
                for k, kmer in enumerate(page_kmers[path[i]]):
                    if i == 0:
                        if kmer == nkmers:
                            kPos = k
                            break
                        continue

                    if kmer == path[i-1]:
                        kPos = k
                        break

                #assert(kPos != -1)
                page_kmers[path[i]][kPos] = path[i+1]
    
    @njit(locals=dict(node=int64, fkmer=int64))
    def _altPath(heap, prev_kmer, prev_page, kmer_pages, page_kmers, ukmer, setB):
        node = prev_page[pagecount]
        fkmer = nkmers
        while prev_page[node] != -1:
            #für prev kmer
            setB(ukmer, prev_kmer[node]*3, 0, 3)
            hfunc = -1
            for k, page in enumerate(kmer_pages[prev_kmer[node]]):
                if page == node:
                    hfunc = k
                    break
            #assert(hfunc != -1)
            setB(ukmer, prev_kmer[node]*3+hfunc, 1)
                
            for k, kmer in enumerate(page_kmers[node]):
                if kmer == fkmer:
                    kPos = k
                    break
            page_kmers[node][kPos] = prev_kmer[node]
            
            fkmer = prev_kmer[node]
            node = prev_page[node]
            
        #für prev kmer
        setB(ukmer, prev_kmer[node]*3, 0, 3)
        hfunc = -1
        for k, page in enumerate(kmer_pages[prev_kmer[node]]):
            if page == node:
                hfunc = k
                break
        #assert(hfunc != -1)
        setB(ukmer, prev_kmer[node]*3+hfunc, 1)
            
        for k, kmer in enumerate(page_kmers[node]):
            if kmer == fkmer:
                kPos = k
                break
        page_kmers[node][kPos] = prev_kmer[node]
        
    @njit(locals=dict(kmer=int64, page=int64))
    def _relax(page, kmer, heap, prev_kmer, prev_page, kmer_pages, page_kmers, count):
        index = -1

        if kmer == nkmers:
            if heap.getVal(page) < heap.getVal(pagecount):
#                print("last root set")
#                print(count)
                prev_page[pagecount] = page
                heap.decreasePriority(pagecount, heap.getVal(page))
#                print(heap.getVal(pagecount))
            return
            
        for i, p in enumerate(kmer_pages[kmer]):
            if p == page:
                index = i+1
                break

        for i, p in enumerate(kmer_pages[kmer]):
            if p == page:
                continue

            if heap.getVal(page)-index+i+1 < heap.getVal(p):
                prev_kmer[p] = kmer
                prev_page[p] = page
                heap.decreasePriority(p, heap.getVal(page)-index+i+1)
        count = 0

    @njit()
    def _dijkstra_scan(start, kmer_pages, page_kmers, heap, prev_kmer, prev_page):
        heap.decreasePriority(kmer_pages[start][2], 3)
        prev_kmer[kmer_pages[start][2]] = start
        heap.decreasePriority(kmer_pages[start][1], 2)
        prev_kmer[kmer_pages[start][1]] = start
        heap.decreasePriority(kmer_pages[start][0], 1)
        prev_kmer[kmer_pages[start][0]] = start

        count = 0
        while heap.min()[0] != -1:
            page, d = heap.pop()
            count += 1
            
            if heap.getVal(pagecount) <= d+1:
                break
                    
            if page == pagecount:
                continue

            for i in page_kmers[page]:
                _relax(page, i, heap, prev_kmer, prev_page, kmer_pages, page_kmers, count)
            
#        print("count:")
#        print(count)
        return heap, prev_kmer, prev_page
        
    @njit()
    def start(heap, prev_kmer, prev_page, kmer_pages, page_kmers, pF, set_bits_at, get_bits_at, ukmer):
        _init(page_kmers, pF, set_bits_at, get_bits_at, ukmer, kmer_pages)
        count = 0
#        print(nkmers)
        for i in range(nkmers):
            if int(get_bits_at(ukmer, i*3, 3)) == 0:
#                if count % 50000 == 0:
#                    print(i)
                count += 1
                _dijkstra_scan(i, kmer_pages, page_kmers, heap, prev_kmer, prev_page)
                _altPath(heap, prev_kmer, prev_page, kmer_pages, page_kmers, ukmer, set_bits_at)
                heap.reset()
                prev_kmer.fill(-1)
                prev_page.fill(-1)

    #only to compile everything one time and get the real runtime    
    heap2 = Heap(pagecount+1)
    prev_kmer2 = np.full(pagecount+1, -1, dtype=np.int64)
    prev_page2 = np.full(pagecount+1, -1, dtype=np.int64)
    kmer_pages2 = kmer_pages.copy()
    page_kmers2 = page_kmers.copy()
    start(heap2, prev_kmer2, prev_page2, kmer_pages2, page_kmers2, pF, set_bits_at, get_bits_at, ukmer)
    pF = np.zeros(pagecount, dtype=np.uint64)
    ukmer = bitarray(nkmers*3, val=0).array
    
    
    beg = datetime.datetime.now()
    heap = Heap(pagecount+1)
    prev_kmer = np.full(pagecount+1, -1, dtype=np.int64)
    prev_page = np.full(pagecount+1, -1, dtype=np.int64)
    start(heap, prev_kmer, prev_page, kmer_pages, page_kmers, pF, set_bits_at, get_bits_at, ukmer)
    end = datetime.datetime.now()
    print("Zeit:")
    print((end-beg))

    print("fertig!")
    val = 0
    for i in range(nkmers*3):
        val+= get_bits_at(ukmer, i)*(i%3+1)
    print("Kosten:")
    print(val)
