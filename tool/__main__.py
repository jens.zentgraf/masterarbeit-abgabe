from argparse import ArgumentParser
from hashfunctions import build_get_page_fpr, get_npages
from build import generate_kmer_iterator
from h5utils import load_from_h5group, save_to_h5group
from bitarray import bitarray
import gc
from intbitarray import intbitarray, IntBitArray

import numpy as np
from numba import njit, int64, uint64, jit

def get_argument_parser():
    p = ArgumentParser()
    p.add_argument("-hf", "--hashfunctions", nargs='+')
    p.add_argument("-ps", "--pagesize", type=int, default = "4")
    p.add_argument("-fr", "--fillrate", type=float, default="0.98")
    p.add_argument("-n", "--objects", type=int)
    p.add_argument("-h5", required=True)
    p.add_argument("-t", "--type", default="alg2", help="Mögliche Parameter: lp1, lpmax, alg1, alg2")
    p.add_argument("-po", "--pageopt", type=float)
    return p
    
def _get_hashfunctions(hashfunctions, npages, universe):
    get_pages_fprs = []
    get_keys = []
    if len(hashfunctions) == 1:
        hashfunctions = hashfunctions[0].split(":")
    for i in hashfunctions:
        get_page_fpr, get_key = build_get_page_fpr(name=i, universe=universe, npages=npages)
        get_pages_fprs.append(get_page_fpr)
        get_keys.append(get_keys)
        
    return get_pages_fprs, get_keys


def _calcPagesForCompKmers(ckmers, getKmers, nkmers, hashfuncs, npages):
    H = np.zeros(shape=(nkmers, 3), dtype=np.uint64)
    
    for i in range(nkmers):
        kmer = getKmers(ckmers, i)
        for j, hashfunc in enumerate(hashfuncs):
            H[i,j], _ = hashfunc(kmer)
        
    L=[[] for _ in range(npages)]
    for i, p in np.ndenumerate(H):
        L[p].append(i)
    return L, H

@njit
def getPagesHung(kmers, nkmers, getKmers, hash0, hash1, hash2):
    H = np.zeros(shape=(nkmers, 3), dtype=np.uint64)
    for i in range(nkmers):
        kmer = getKmers(kmers, i)
        H[i, 0], _ = hash0(kmer)
        H[i, 1], _ = hash1(kmer)
        H[i, 2], _ = hash2(kmer)
    return H   

def main(args):
    print("start")
    print("read h5 file")
    info = load_from_h5group(args.h5, "info")["info"]
    data = load_from_h5group(args.h5, "data")
         
    kmers = data['kmercodes']
    nkmers = info['kmers']
    pagesize =info['pagesize']
    npages = int(info['npages'])
    universe = int(info['universe'])
    get_pages_fprs, get_keys = _get_hashfunctions(info['hashfuncs'].decode().split(':'), int(info['npages']), int(info['universe']))
    k = int(info['k'])
    choices = None
    hashfuncs = info['hashfuncs'].decode().split(':')
    
    if args.fillrate:
        fr = args.fillrate
        
    if args.pagesize:
        pagesize = args.pagesize
        info['pagesize'] = pagesize
    
    if args.hashfunctions:
        hashfuncs = args.hashfunctions
        info['hashfuncs'] = ":".join(hashfuncs).encode()
        
    if args.fillrate or args.pagesize:
        npages = get_npages(nkmers, pagesize, fr)
        universe = 4**k
        get_pages_fprs, get_keys = _get_hashfunctions(hashfuncs, npages, universe)
        info['npages'] = npages
        
    if args.fillrate or args.pagesize or args.hashfunctions:
        get_pages_fprs, get_keys = _get_hashfunctions(hashfuncs, npages, universe)
        
    print("-----------INFO----------")
    print(info)
    print("pagesize:")
    print(info['pagesize'])
    print("hashfunctions:")
    print(info['hashfuncs'].decode())
    print("fillrate:")
    print(fr)
    print("------------------------")
        
    
    if args.type == "lp1":
        from lp_xij_with_Y import optimize
        ckmerss = intbitarray(nkmers, 2*k, init=kmers)
        ckmers = ckmerss.array
        getKmers = ckmerss.get
        print("calc L,H")
        L, H = _calcPagesForCompKmers(ckmers, getKmers, nkmers, tuple(get_pages_fprs), npages)
        print("run Opt")
        pageopt = False
        if args.pageopt:
            pageopt = True
        
        status, choices = optimize(L, nkmers, pagesize, pageopt, args.pageopt, H, False, choices)
        
    if args.type == "lpmax":
        from lp_max1 import optimize
        ckmerss = intbitarray(nkmers, 2*k, init=kmers)
        ckmers = ckmerss.array
        getKmers = ckmerss.get
        print("calc L,H")
        L, H = _calcPagesForCompKmers(ckmers, getKmers, nkmers, tuple(get_pages_fprs), npages)
        print("run Opt")
        status, choices = optimize(L, nkmers, pagesize, H)
        
    elif args.type == "alg1":
        from alg1 import optimize
        
        ckmerss = intbitarray(nkmers, 2*k, init=kmers)
        ckmers = ckmerss.array
        getKmers = ckmerss.get
        
        pages = getPagesHung(ckmers, nkmers, getKmers, get_pages_fprs[0], get_pages_fprs[1], get_pages_fprs[2])
        optimize(pages, pagesize, npages)
        
    elif args.type == "alg2":
        from iter_pages import optimize
        ckmers = intbitarray(nkmers, 2*k, init=kmers)
        del(data)
        gc.collect()
        gpf = tuple(get_pages_fprs)
        print("start opt")
        print(k)
        choices = optimize(nkmers, pagesize, npages, gpf, ckmers, k)


if __name__ == "__main__":
    p = get_argument_parser()
    args = p.parse_args()
    main(args)
