import numpy as np
from bitarray import bitarray
from numba import njit, jit, uint64, int64, uint32, uint8, boolean
import datetime
from intbitarray import intbitarray


def optimize(nkmers, pagesize, pagecount, hfs, kmers, k):
    pFF = intbitarray(pagecount, 4)
    pF = pFF.array
    getpagefill = pFF.get
    setpagefill = pFF.set

    
    # Wenn eine Kante benutzt wurde wird das Bit auf 1 gesetzt, jedes kmer hat 3 bits für die Hashfunktionen
    ukmerr = bitarray(nkmers*2, val=0)
    ukmer = ukmerr.array
    get_bits_at = ukmerr.get  # (array, startbit, nbits=1)
    set_bits_at = ukmerr.set
    
    @jit()
    def calcStats(get_bits_at, ukmer, pF):
        val = 0
        choiceStat = np.zeros(3, dtype=np.uint64)
        for i in range(nkmers):
            choice = int(get_bits_at(ukmer, i*2, 2))
            val+= choice
            choiceStat[choice-1] +=1
        print("costs:")
        print(val)
        print(val/nkmers)
        print("choice Stat:")
        print("1: ", choiceStat[0])
        print("2: ", choiceStat[1])
        print("3: ", choiceStat[2])
        pageFillStat = np.zeros(pagesize+1, dtype = np.uint64)
        for i in range(pagecount):
            pageFillStat[getpagefill(pF, i)] += 1

        print("page fill:")
        for i, j in enumerate(pageFillStat):
            print(str(i)+": " + str(j))


    @njit
    def checkL(Li, Lv, Lh, ukmer, get, set, nkmers, pagecount):
        for i in range(0, Li[-1]):
            if Lv[i] != np.iinfo(uint64).max:
                choice = int(get(ukmer, Lv[i]*2, 2))
                hf = int(get(Lh, i*2, 2))
                #''assert(choice != hf)
    
            
    @njit(nogil=True, locals=dict(kmer=uint64, i=uint32))
    def getL(nkmers, k, kmers, get_int_at, hfs, pagecount, hf, ukmer):

        ind = np.zeros(pagecount+1, dtype = np.uint64)# 7,5milliarden geht nicht mit uint32d
        set = sethashat
        get = gethashat

        mFill = np.zeros(pagecount, dtype=np.uint64)
        for i in range(nkmers):
            choice = int(get_bits_at(ukmer, i*2, 2))
            if choice == 0:
                h1 = hfs[0](int(get_int_at(kmers, i)))[0]
                h2 = hfs[1](int(get_int_at(kmers, i)))[0]
                h3 = hfs[2](int(get_int_at(kmers, i)))[0]
                mFill[h1] += 1
                if h2 != h1:
                    mFill[h2] += 1
                if h3 != h1 and h3 != h2:
                    mFill[h3] += 1
            elif choice == 1:
                h1 = hfs[0](int(get_int_at(kmers, i)))[0]
                h2 = hfs[1](int(get_int_at(kmers, i)))[0]
                h3 = hfs[2](int(get_int_at(kmers, i)))[0]
                if h2 != h1:
                    mFill[h2] += 1
                if h3 != h2 and h3 != h1:
                    mFill[h3] += 1
            elif choice == 2:
                h1 = hfs[0](int(get_int_at(kmers, i)))[0]
                h2 = hfs[1](int(get_int_at(kmers, i)))[0]
                h3 = hfs[2](int(get_int_at(kmers, i)))[0]
                mFill[h1] += 1
                if h3 != h1 and h3 != h2:
                    mFill[h3] += 1
            elif choice == 3:
                #''assert()
                h1 = hfs[0](int(get_int_at(kmers, i)))[0]
                h2 = hfs[1](int(get_int_at(kmers, i)))[0]
                mFill[h1] += 1
                if h2 != h1:
                    mFill[h2] += 1
                    
        for i in range(1, pagecount):
            ind[i] = ind[i-1] + mFill[i-1]
        ind[pagecount] = ind[pagecount-1]+mFill[pagecount-1]
        
        val = np.zeros(ind[pagecount], dtype = np.uint32)#TODO 32sollte reichen??

        pFill = np.zeros(pagecount, dtype=np.uint32)# uint8 ist zu klein!
        for i in range(nkmers):
            if int(get_bits_at(ukmer, i*2, 2)) == 0:
                h1 = hfs[0](int(get_int_at(kmers, i)))[0]
                h2 = hfs[1](int(get_int_at(kmers, i)))[0]
                h3 = hfs[2](int(get_int_at(kmers, i)))[0]
                
                #''assert(val[ind[h1]+pFill[h1]] == 0)
                val[ind[h1]+pFill[h1]] = i
                #''assert(int(get(hf, (ind[h1]+pFill[h1])*2, 2))==0)
                set(hf, (ind[h1]+pFill[h1]), 1)
                pFill[h1] += 1
                
                if h2 != h1:
                    #''assert(val[ind[h2]+pFill[h2]] == 0)
                    val[ind[h2]+pFill[h2]] = i
                    #''assert(int(get(hf, (ind[h2]+pFill[h2])*2, 2))==0)
                    set(hf, (ind[h2]+pFill[h2]), 2)
                    pFill[h2] += 1
                if h3 != h1 and h3 != h2:
                    #''assert(val[ind[h3]+pFill[h3]] == 0)
                    val[ind[h3]+pFill[h3]] = i
                    #''assert(int(get(hf, (ind[h3]+pFill[h3])*2, 2))==0)
                    set(hf, (ind[h3]+pFill[h3]), 3)
                    pFill[h3] += 1
                    
            elif int(get_bits_at(ukmer, i*2, 2)) == 1:
                h1 = hfs[0](int(get_int_at(kmers, i)))[0]
                h2 = hfs[1](int(get_int_at(kmers, i)))[0]
                h3 = hfs[2](int(get_int_at(kmers, i)))[0]
                
                if h2 != h1:
                    #''assert(val[ind[h2]+pFill[h2]] == 0)
                    val[ind[h2]+pFill[h2]] = i
                    #''assert(int(get(hf, (ind[h2]+pFill[h2])*2, 2))==0)
                    set(hf, (ind[h2]+pFill[h2]), 2)
                    pFill[h2] += 1
                
                if h3 != h2 and h3 != h1:
                    #''assert(val[ind[h3]+pFill[h3]] == 0)
                    val[ind[h3]+pFill[h3]] = i
                    #''assert(int(get(hf, (ind[h3]+pFill[h3])*2, 2))==0)
                    set(hf, (ind[h3]+pFill[h3]), 3)
                    pFill[h3] += 1
            elif int(get_bits_at(ukmer, i*2, 2)) == 2:
                h1 = hfs[0](int(get_int_at(kmers, i)))[0]
                h2 = hfs[1](int(get_int_at(kmers, i)))[0]
                h3 = hfs[2](int(get_int_at(kmers, i)))[0]
                
                #''assert(val[ind[h1]+pFill[h1]] == 0)
                val[ind[h1]+pFill[h1]] = i
                #''assert(int(get(hf, (ind[h1]+pFill[h1])*2, 2))==0)
                set(hf, (ind[h1]+pFill[h1]), 1)
                pFill[h1] += 1
                
                if h3 != h1 and h3 != h2:
                    #''assert(val[ind[h3]+pFill[h3]] == 0)
                    val[ind[h3]+pFill[h3]] = i
                    #''assert(int(get(hf, (ind[h3]+pFill[h3])*2, 2))==0)
                    set(hf, (ind[h3]+pFill[h3]), 3)
                    pFill[h3] += 1
                    
            elif int(get_bits_at(ukmer, i*2, 2)) == 3:
                assert()
                
        mFill = None
        pFill = None 

        return ind, val, hf
    
    @njit(nogil=True, locals=dict(page=uint64, pageFill=uint8))
    def _init(pF, pagecount, pagesize, set_bits_at, get_bits_at, ukmer, hfs, kmers, get_int_at, nkmers, k):
        for i in range(nkmers):
            #''assert(int(get_bits_at(ukmer, i*2, 2)) == 0)
            page = hfs[0](int(get_int_at(kmers, i)))[0]
            pageFill = getpagefill(pF, page)
            if pageFill != pagesize:
                setpagefill(pF, page, pageFill+1)
                set_bits_at(ukmer, i*2, 1, 2)

        count = 0
        for i in range(nkmers):
            if int(get_bits_at(ukmer, i*2, 2)) == 0:
                page = hfs[1](int(get_int_at(kmers, i)))[0]
                pageFill = getpagefill(pF, page)
                if pageFill != pagesize:
                    setpagefill(pF, page, pageFill+1)
                    set_bits_at(ukmer, i*2, 2, 2)
                else:
                    count += 1
                    
        
        print(count)
        return count

    @njit(nogil=True)
    def alternatePaths(prev_kmer, prev_page, set_bits_at, get_bits_at, ukmer, pF, pagecost, visitedKmer, hfs, kmers, get_int_at, nkmers, k, Li, Lh, Lv):
        count = 0

        for skmer in range(nkmers):
            #hat es keine choice (=0) ist es ein offenes kmer
            if int(get_bits_at(ukmer, skmer*2, 2)) != 0:
                continue

            visited = False
            node = skmer
            while node != np.iinfo(np.uint32).max:
                if int(get_bits_at(visitedKmer, node)):
                    visited = True
                    break

                #die page war mal frei, allerdings wurd in dieser iteration bereits ein kmer darauf abgebildet
                page_choice = int(get_bits_at(prev_page, node*2, 2))
                if page_choice == 1:
                    ppage = hfs[0](int(get_int_at(kmers, node)))[0]
                elif page_choice == 2:
                    ppage = hfs[1](int(get_int_at(kmers, node)))[0]
                elif page_choice == 3:
                    ppage = hfs[2](int(get_int_at(kmers, node)))[0]
                    
                if prev_kmer[ppage] == np.iinfo(np.uint32).max:
                    if getpagefill(pF, ppage) == pagesize:
                        visited = True
                        break

                node = prev_kmer[ppage]

            if visited:
                continue

            count += 1
            node = skmer
            while node != np.iinfo(np.uint32).max:
                set_bits_at(visitedKmer, node, 1)
                if int(get_bits_at(prev_page, node*2, 2)) == 1:
                    #''assert(oc != 1)
                    set_bits_at(ukmer, node*2, 1, 2)
                elif int(get_bits_at(prev_page, node*2, 2)) == 2:
                    #''assert(oc != 2)
                    set_bits_at(ukmer, node*2, 2, 2)
                elif int(get_bits_at(prev_page, node*2, 2)) == 3:
                    #''assert(oc != 3)
                    set_bits_at(ukmer, node*2, 3, 2)
                else:
                    assert()

                page_choice = int(get_bits_at(prev_page, node*2, 2))
                if page_choice == 1:
                    ppage = hfs[0](int(get_int_at(kmers, node)))[0]
                elif page_choice == 2:
                    ppage = hfs[1](int(get_int_at(kmers, node)))[0]
                elif page_choice == 3:
                    ppage = hfs[2](int(get_int_at(kmers, node)))[0]
                #seite hat noch platz => Ende; einfügen
                pageFill = getpagefill(pF, ppage)
                if pageFill != pagesize:
                    for i in range(Li[ppage], Li[ppage+1]):
                        if Lv[i] == node:
                            setpagefill(pF, ppage, pageFill+1)
                            Lv[i] = np.iinfo(np.uint32).max
                            sethashat(Lh, i, 0)
                            break
                    break

                #''assert(pF[ppage] == pagesize)
                for i in range(Li[ppage], Li[ppage+1]):
                    if Lv[i] == node:
                        Lv[i] = prev_kmer[ppage]
#                        #''assert(int(get_bits_at(Lh, i*2, 2)) == int(get_bits_at(ukmer, node*2,2)))
                        choice = int(get_bits_at(ukmer, prev_kmer[ppage]*2, 2))
                        sethashat(Lh, i, choice)
                        #''assert(prev_kmer[ppage] != np.iinfo(np.uint32).max)
                        break
                    
                node = prev_kmer[ppage]

        return count

    @njit(nogil=True, locals=dict(i=int64, changes=boolean, kmer=uint32, page=uint32, choice=uint8, hashfunc=uint8, prevPage = uint32))
    def findPaths(Lv, Li, Lh, pagecost, pagecount, prev_page, prev_kmer, set_bits_at, get_bits_at, ukmer, activePage, pF, hfs, kmers, get_int_at, nkmers, k):
        
        #alle Seiten mit Platz auf aktiv setzen
        for i in range(pagecount):
            val = getpagefill(pF, i)
            if val < pagesize:
                pagecost[i] = 0
                set_bits_at(activePage, i, 1)

        changes = True
        #solange sich in der letzten Itartion etwas geändert hat
        count = 0
        while changes:
            changes = False
            for page in range(pagecount):
                #Die Seite ist inaktiv
                if int(get_bits_at(activePage, page)) == 0:
                    continue

                set_bits_at(activePage, page, 0)
                count += 1
                for i in range(Li[page], Li[page+1]):
                    kmer = Lv[i]
                    
                    if kmer == np.iinfo(np.uint32).max:
                        continue
                        
                    hashfunc = int(gethashat(Lh, i))
                    #''assert(hashfunc != 0)

                    #''assert(int(get_bits_at(ukmer, kmer*2, 2)) != hashfunc)      

                    choice = int(get_bits_at(ukmer, kmer*2, 2))
                    
                    #TODO: hfs[choice-1] numba error
                    if choice == 0:
                        if int(get_bits_at(prev_page, kmer*2, 2)) == 0:
                            set_bits_at(prev_page, kmer*2, hashfunc, 2)
                        else:
                            prevPageHashfunc = int(get_bits_at(prev_page, kmer*2, 2))
                            if prevPageHashfunc == 1:
                                prevPage = hfs[0](int(get_int_at(kmers, kmer)))[0]
                            elif prevPageHashfunc == 2:
                                prevPage = hfs[1](int(get_int_at(kmers, kmer)))[0]
                            elif prevPageHashfunc == 3:
                                prevPage = hfs[2](int(get_int_at(kmers, kmer)))[0]
                            if pagecost[page]+hashfunc < pagecost[prevPage]+prevPageHashfunc:
                                set_bits_at(prev_page, kmer*2, hashfunc, 2)
                        continue
                    elif choice == 1:
                        choicePage = hfs[0](int(get_int_at(kmers, kmer)))[0]
                    elif choice == 2:
                        choicePage = hfs[1](int(get_int_at(kmers, kmer)))[0]
                    elif choice == 3:
                        choicePage = hfs[2](int(get_int_at(kmers, kmer)))[0]
                    else:
                        assert()
                        
                    if pagecost[choicePage] <= pagecost[page] -choice+hashfunc:
                        continue

                    set_bits_at(prev_page, kmer*2, hashfunc, 2)
                    
                    set_bits_at(activePage, choicePage, 1)
                    pagecost[choicePage] = pagecost[page] -choice + hashfunc
                    prev_kmer[choicePage] = kmer
                    changes = True

        print("betrachtete Seiten: ", count)

    @njit(nogil=True)
    def start(pF, pagecount, pagesize, set_bits_at, get_bits_at, ukmer, visitedKmer, activePage, hfs, kmers, get_int, k, nkmers, hf, prev_page):
        pagecost = np.full(pagecount, np.iinfo(np.int16).max, dtype=np.int16)
        nok = _init(pF, pagecount, pagesize, set_bits_at, get_bits_at, ukmer, hfs, kmers, get_int, nkmers, k)
        
      
        
        print("jetzt das neue L")
        Li, Lv, Lh = getL(nkmers, k, kmers, get_int, hfs, pagecount, hf, ukmer)

#        checkL(Li, Lv, Lh, ukmer, get_bits_at, set_bits_at, nkmers, pagecount)
#        brauch ich für die vorgänger der offenen seiten....
        prev_kmer = np.full(pagecount, np.iinfo(np.uint32).max, dtype=np.uint32)# uint32 sollte reichen
        while nok > 0:
            print("findPaths")
            findPaths(Lv, Li, Lh, pagecost, pagecount, prev_page, prev_kmer, set_bits_at, get_bits_at, ukmer, activePage, pF, hfs, kmers, get_int, nkmers, k)
            print("alternatePaths")
#            checkL(Li, Lv, Lh, ukmer, get_bits_at, set_bits_at, nkmers, pagecount)
            insertedKmer = alternatePaths(prev_kmer, prev_page, set_bits_at, get_bits_at, ukmer, pF, pagecost, visitedKmer, hfs, kmers, get_int, nkmers, k, Li, Lh, Lv)
            print("inserted: ", insertedKmer)
#            checkL(Li, Lv, Lh, ukmer, get_bits_at, set_bits_at, nkmers, pagecount)
            nok -= insertedKmer
            print("nok: ", nok)
            pagecost.fill(np.iinfo(np.int16).max)
            prev_kmer.fill(np.iinfo(np.uint32).max)

            for i in range(nkmers):
                set_bits_at(visitedKmer, i, 0)
                set_bits_at(prev_page, i, 0, 2)
            for i in range(pagecount):
                set_bits_at(activePage, i, 0)
            if insertedKmer == 0:
                print("unsolvable")
                assert()

    visitedKmer = bitarray(nkmers, val=0).array
    activePage = bitarray(pagecount, val=0).array
    prev_page = bitarray(nkmers*2, val=0).array
    hff = intbitarray(nkmers*3, 2)#TODO *3*2 ist der worst case vielleicht gehts anders
    hf = hff.array
    gethashat = hff.get
    sethashat = hff.set
    
    beg = datetime.datetime.now()
    start(pF, pagecount, pagesize, set_bits_at, get_bits_at, ukmer, visitedKmer, activePage, hfs, kmers.array, kmers.get, k, nkmers, hf, prev_page)
    
    end = datetime.datetime.now()
    print((end-beg).total_seconds())

    print("fertig!")
    calcStats(get_bits_at, ukmer, pF)
    return ukmer
