from gurobipy import *
import datetime
import numpy as np

def optimize(L,nkmer, pagesize, optpage=False, beta=1, kmer_pages=None, usestart=False, startvalue=None):
    
    m = Model("opt_hash")
    m.setParam("Method", 1)
    
    print("Adding variables")
    #create variables
    start = datetime.datetime.now()
    vars = m.addVars(nkmer,3, vtype=GRB.BINARY)
    y = m.addVars(len(L), lb=0, ub=pagesize, vtype=GRB.INTEGER)
    end = datetime.datetime.now()
    print("Adding variables: %s"%(end-start))
    
    print("Update Model")
    #update model
    start = datetime.datetime.now()
    m.update()
    end = datetime.datetime.now()
    print("Update Model: %s"%(end-start))
    
    if usestart:
        for i,j in enumerate(startvalue):
            vars[i,j].Start=1.0
    
    
    print("Set Objective")
    #minimize
    start = datetime.datetime.now()
    m.setObjective(quicksum(vars[i,j]*(1+j  ) for i in range(nkmer) for j in range(3)), GRB.MINIMIZE)
    end = datetime.datetime.now()
    print("Set Objective: %s"%(end-start))  
    
    print("Adding kmer constraints")
    #constraint for kmers
    start = datetime.datetime.now()
    m.addConstrs(quicksum(vars[i,j] for j in range(3)) == 1 for i in range(nkmer))
                              
    print("Adding page constraints")
    #constraints for pages
    m.addConstrs((quicksum(vars[i,j] for (i,j) in L[p]) +y[p] == pagesize for p in range(len(L)) if len(L[p]) >= pagesize ))
    end = datetime.datetime.now()
    print("Add Constraints: %s"%(end-start))
    
    start = datetime.datetime.now()
    m.write("file.lp.gz")
    end = datetime.datetime.now()
    print("Write Model: %s"%(end-start))
    
    start = datetime.datetime.now()
    m.optimize()
    end = datetime.datetime.now()
    print("Optimize Model: %s"%(end-start))
    
    #ausgaben
    one=0
    two=0
    three=0
    for var in vars:
        if vars[var].obj == 1:
           one += vars[var].X
        elif vars[var].obj == 2:
           two += vars[var].X
        elif vars[var].obj == 3:
           three += vars[var].X 
    print("kmers:"+str(len(vars)))
    print("choiche statistics:")
    print("1:"+str(one)+":"+str(100/nkmer*one)+"%")
    print("2:"+str(two)+":"+str(100/nkmer*two)+"%")
    print("3:"+str(three)+":"+str(100/nkmer*three)+"%")
    print("\npage fill level:")
    pagefill = (pagesize+1)*[0]
    for page in L:
        fill = 0
        for (i,j) in page:
            fill += vars[i,j].X
        pagefill[int(fill)]+=1
    print("pagesize: "+str(pagesize))
    for i in range(len(pagefill)):
        print("%s: %s" % (i, pagefill[i]))
        
    if optpage:
        # zweites LP
        print("start pageopt")
        print("Beta: ", beta)
        obj= one+2*two+3*three
        print("addVars")
        z = m.addVars(len(L), vtype=GRB.BINARY)
        m.update
        
        # z initialisieren TODO: val[klammern  zähler] das geht nicht...
        print("init Vars")
        start = datetime.datetime.now()
        for i, val in enumerate(y):
#            print(y[val])
            if y[val].X > 0.0:
                z[i].start=1.0
        end = datetime.datetime.now()
        print("Init Vars: %s"%(end-start))
        
        print("Add Constrs")
        start = datetime.datetime.now()
        m.addConstrs(z[i] <= y[i] for i in range(len(z)))
        m.addConstr(quicksum(vars[i,j]*(1+j) for i in range(nkmer) for j in range(3)) <= beta*obj)
        #fuer dicht befüllte Seiten:
        m.addConstrs(quicksum(vars[i,j] for (i,j) in L[kmer_pages[k][0]]) >= vars[k,1]*pagesize for k in range(nkmer))
        
        m.addConstrs(quicksum(vars[i,j] for (i,j) in L[kmer_pages[k][0]]) + quicksum(vars[i,j] for (i,j) in L[kmer_pages[k][1]]) >= vars[k,2]*pagesize*2 for k in range(nkmer))
        
        end = datetime.datetime.now()
        print("Add Constrs: %s"%(end-start))
        
        print("Set Objective")
        start = datetime.datetime.now()
        m.setObjective(quicksum(z), GRB.MAXIMIZE)
        #m.write("file2.lp")
        end = datetime.datetime.now()
        print("Set Objective : %s"%(end-start))
        m.setParam("Method", -1)
        #m.tune()
        print("optimize")
        start = datetime.datetime.now()
        m.optimize()
        end = datetime.datetime.now()
        print("Opitmize Pages: %s"%(end-start))
        m.setParam("Method", -1)
        
        #ausgaben
        one=0
        two=0
        three=0
        #obj nicht gesetzt, da nicht in funktion enthalten; auf j index umstellen
        for i in range(nkmer):
            one += vars[i,0].X
            two += vars[i,1].X
            three += vars[i,2].X            
        print("kmers:"+str(len(vars)))
        print("choiche statistics pageopt:")
        print("1:"+str(one)+":"+str(100/nkmer*one)+"%")
        print("2:"+str(two)+":"+str(100/nkmer*two)+"%")
        print("3:"+str(three)+":"+str(100/nkmer*three)+"%")
        print("\npage fill level pageopt:")
        pagefill = (pagesize+1)*[0]
        for page in L:
            fill = 0
            for (i,j) in page:
                fill += vars[i,j].X
            pagefill[int(fill)]+=1
        print("pagesize: "+str(pagesize))
        for i in range(len(pagefill)):
            print("%s: %s" % (i, pagefill[i]))
            
    choices = np.zeros(nkmer, dtype=np.uint8)
    for i in range(nkmer):
        choices[i] = 0*vars[i,0].X+1*vars[i,1].X+2*vars[i,2].X
        
    return m.Status, choices
