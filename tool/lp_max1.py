from gurobipy import *
import datetime
import numpy as np

def optimize(L,nkmer, pagesize, kmer_pages):
    
    m = Model("opt_hash")
    m.setParam("Method", 1)
    
    print("Adding variables")
    #create variables
    start = datetime.datetime.now()
    vars = m.addVars(nkmer,3, vtype=GRB.BINARY)
    y = m.addVars(len(L), lb=0, ub=pagesize, vtype=GRB.INTEGER)
    end = datetime.datetime.now()
    print("Adding variables: %s"%(end-start))
    
    print("Update Model")
    #update model
    start = datetime.datetime.now()
    m.update()
    end = datetime.datetime.now()
    print("Update Model: %s"%(end-start))
    
    print("Set Objective")
    #minimize
    m.setObjective(quicksum(vars[i,0] for i in range(nkmer)), GRB.MAXIMIZE)
    #m.setObjective(quicksum(vars[i,0] for i in range(nkmer)), GRB.MAXIMIZE)    
    
    print("Adding kmer constraints")
    #constraint for kmers
    m.addConstrs(quicksum(vars[i,j] for j in range(3)) == 1 for i in range(nkmer))
    
                          
    print("Adding page constraints")
    #constraints for pages
    m.addConstrs((quicksum(vars[i,j] for (i,j) in L[p]) +y[p] == pagesize for p in range(len(L))))
    
    #constraint to fill pages 1 and 2 completely before using page 3
    m.addConstrs(quicksum(vars[i,j] for (i,j) in L[kmer_pages[k][0]]) + quicksum(vars[i,j] for (i,j) in L[kmer_pages[k][1]]) >= vars[k,2]*pagesize*2 for k in range(nkmer))
    
    m.write("file.lp.gz")
    m.optimize()
    
    #ausgaben
    one=0
    two=0
    three=0
    for i in range(nkmer):
           one += vars[i,0].X
           two += vars[i,1].X
           three += vars[i,2].X 
    print("kmers:"+str(len(vars)))
    print("choiche statistics:")
    print("1:"+str(one)+":"+str(100/nkmer*one)+"%")
    print("2:"+str(two)+":"+str(100/nkmer*two)+"%")
    print("3:"+str(three)+":"+str(100/nkmer*three)+"%")
    print("\npage fill level:")
    pagefill = (pagesize+1)*[0]
    for page in L:
        fill = 0
        for (i,j) in page:
            fill += vars[i,j].X
        pagefill[int(fill)]+=1
    print("pagesize: "+str(pagesize))
    for i in range(len(pagefill)):
        print("%s: %s" % (i, pagefill[i]))
    choices = np.zeros(nkmer, dtype=np.uint8)
    for i in range(nkmer):
        choices[i] = 0*vars[i,0].X+1*vars[i,1].X+2*vars[i,2].X
        
    return m.Status, choices
